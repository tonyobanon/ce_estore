
const fs = require('fs');
const { Writable } = require('stream');

const toArray = require('stream-to-array')
const Jimp = require('jimp');

exports.install = function () {
	// COMMON
	F.route('/', view_homepage);
	F.route('#contact', view_contact);

	// UI tests
	F.route('/design/', 'design');

	// CMS rendering
	F.route('/*', view_page);

	// FILES
	F.file('/images/small/*.jpg', file_image);
	F.file('/images/large/*.jpg', file_image);
	F.file('/download/', file_read);

	// ERRORS
	F.route('#404', view_404);
	F.route('#401', view_401);
};

// ==========================================================================
// COMMON
// ==========================================================================

// Homepage
function view_homepage() {
	var self = this;

	// Increases the performance (1 minute cache)
	self.memorize('cache.homepage', '1 minute', DEBUG, function () {
		var options = {};
		options.max = 12;
		options.sort = '3';
		GETSCHEMA('Product').query(options, function (err, response) {
			// Finds "homepage"
			self.page('/', 'index', response, false, true);
		});
	});
}

// Contact with contact form
function view_contact() {
	var self = this;
	self.render(self.url);
}

// ==========================================================================
// CMS (Content Management System)
// ==========================================================================

function view_page() {
	var self = this;
	// models/pages.js --> Controller.prototype.render()
	self.render(self.url);
}

// ==========================================================================
// FILES
// ==========================================================================

// Reads a specific file from database
// For images (jpg, gif, png) supports percentual resizing according "?s=NUMBER" argument in query string e.g.: .jpg?s=50, .jpg?s=80 (for image galleries)
// URL: /download/*.*
function file_read(req, res) {

	var id = req.split[1].replace('.' + req.extension, '');

	if (!req.query.s || (req.extension !== 'jpg' && req.extension !== 'gif' && req.extension !== 'png')) {
		// Reads specific file by ID
		F.exists(req, res, function (next, filename) {
			NOSQL('files').binary.read(id, function (err, stream, header) {

				if (err) {
					next();
					return res.throw404();
				}

				var writer = require('fs').createWriteStream(filename);

				CLEANUP(writer, function () {
					res.file(filename);
					next();
				});

				stream.pipe(writer);
			});
		});
		return;
	}

	// Custom image resizing
	var size;

	// Small hack for the file cache.
	// F.exists() uses req.uri.pathname for creating temp identificator and skips all query strings by creating (because this hack).
	if (req.query.s) {
		size = req.query.s.parseInt();
		req.uri.pathname = req.uri.pathname.replace('.', size + '.');
	}

	// Below method checks if the file exists (processed) in temporary directory
	// More information in total.js documentation
	F.exists(req, res, 10, function (next, filename) {

		// Reads specific file by ID
		NOSQL('files').binary.read(id, function (err, stream, header) {

			if (err) {
				next();
				return res.throw404();
			}

			toArray(stream)
				.then(function (parts) {
					var buffers = []
					for (var i = 0, l = parts.length; i < l; ++i) {
						var part = parts[i]
						buffers.push((part instanceof Buffer) ? part : new Buffer(part))
					}
					return Buffer.concat(buffers)
				}).then((imageBuffer) => {
					processImage(imageBuffer);
				});

			function processImage(imageBuffer) {

				Jimp.read(imageBuffer, function (err, image) {

					if (err) throw err;

					req.extension === 'jpg' && image.quality(85);

					let height = (size / 100) * image.bitmap.height;
					let width = (size / 100) * image.bitmap.width;

					size && image.resize(width, height, Jimp.RESIZE_BEZIER);
					//.minify();

					//req.extension
					image.getBuffer(Jimp.MIME_JPEG, function (err, buffer) {

						// Releases F.exists()
						next();

						// Write to file
						var writer = require('fs').createWriteStream(filename);
						writer.write(buffer);
						writer.end();

						res.contentType = `${image.getMIME()}`;
						res.end(buffer);
					});

				});
			}
		});
	});
}

// Reads specific picture from database
// URL: /images/small|large/*.jpg
function file_image(req, res) {

	// Below method checks if the file exists (processed) in temporary directory
	// More information in total.js documentation
	F.exists(req, res, 10, function (next, filename) {

		let file_id = req.split[2].replace('.jpg', '');

		// Reads specific file by ID
		NOSQL('files').binary.read(file_id, function (err, stream, header) {

			if (err) {
				next();
				return res.throw404();
			}

			toArray(stream)
				.then(function (parts) {
					var buffers = []
					for (var i = 0, l = parts.length; i < l; ++i) {
						var part = parts[i]
						buffers.push((part instanceof Buffer) ? part : new Buffer(part))
					}
					return Buffer.concat(buffers)
				}).then((imageBuffer) => {
					processImage(imageBuffer);
				});

			function processImage(imageBuffer) {

				Jimp.read(imageBuffer, function (err, image) {

					if (err) throw err;

					image.quality(90);

					if (req.split[1] === 'large')
						//500
						image.resize(Jimp.AUTO, 300);
					else
						//200
						image.resize(Jimp.AUTO, 150);

					//.minify();

					//req.extension
					image.getBuffer(Jimp.MIME_JPEG, function (err, buffer) {

						// Releases F.exists()
						next();

						// Write to file
						var writer = require('fs').createWriteStream(filename);
						writer.write(buffer);
						writer.end();

						res.contentType = `${image.getMIME()}`;
						res.end(buffer);
					});

				});
			}
		});
	});
}

// ==========================================================================
// ERRORS
// ==========================================================================

function view_404() {
	var self = this;
	self.view('404');
}

function view_401() {
	var self = this;
	self.view('401');
}
